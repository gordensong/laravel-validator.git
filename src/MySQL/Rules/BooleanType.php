<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;
use Illuminate\Support\Str;

class BooleanType extends BaseRuleGetter
{
	public function rules(Column $column): array
	{
		$schemaType = $this->getSchemaType($column);

		if (Str::startsWith($schemaType, 'tinyint(1)')) {
			$rules[] = 'boolean';
		} else {
			$rules[] = 'integer';

			if ($column->getUnsigned()) {
				$rules[] = 'min:0';
				$rules[] = 'max:255';
			} else {
				$rules[] = 'min:-127';
				$rules[] = 'max:128';
			}
		}

		return $rules;
	}
}
