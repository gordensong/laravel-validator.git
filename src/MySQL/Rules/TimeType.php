<?php


namespace GordenSong\Laravel\MySQL\Rules;


use Doctrine\DBAL\Schema\Column;

class TimeType extends BaseRuleGetter
{
	const REGEX = 'regex:/^\d{1,2}:\d{1,2}:\d{1,2}(\.\d{1,6})?$/i';

	public function rules(Column $column): array
	{
		return [self::REGEX];
	}
}