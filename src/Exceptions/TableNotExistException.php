<?php


namespace GordenSong\Laravel\Exceptions;


use Throwable;

class TableNotExistException extends \Exception
{
	public function __construct($table, $code = 0, Throwable $previous = null)
	{
		parent::__construct("Table({$table}) don't exist", $code, $previous);
	}
}